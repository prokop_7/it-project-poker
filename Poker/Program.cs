﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using static System.Console;

namespace Poker
{
    class Program
    {
        public static readonly List<Card> Deck = new List<Card>();
        public static readonly Random R = new Random();
        public const int HandSize = 5;

        public static void Main(string[] args)
        {
//            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
//                Environment.Exit(1);
            IPEndPoint[] v = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners();
            foreach (IPEndPoint ipEndPoint in v)
            {
                if (ipEndPoint.Port != int.Parse(args[0])) continue;
                WriteLine("Error");
                ReadLine();
                Environment.Exit(1);
            }
            OutputEncoding = System.Text.Encoding.Unicode;
            InputEncoding = System.Text.Encoding.Unicode;
            SetDeck();
            goto Server;

            List<Card> deck = new List<Card>(Deck);
            List<Card> userCards = new List<Card>();
            List<Card> oppCards = new List<Card>();
            List<Card> desk = new List<Card>();
            WriteLine("Enter user cards");
            userCards.Add(new Card(ReadLine()));
            userCards.Add(new Card(ReadLine()));

            WriteLine("Enter opp cards");
            oppCards.Add(new Card(ReadLine()));
            oppCards.Add(new Card(ReadLine()));

            string s;
            WriteLine("Enter desk cards");
            while ((s = ReadLine()) != "")
            {
                desk.Add(new Card(s));
            }
            Hand h = new Hand(Probability.MergeCard(desk, oppCards));
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Probability p = new Probability(userCards, desk, deck, oppCards);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            WriteLine("Total = " + ts + "\n\n");
            WriteLine("Desk = " + PrintCards(desk));
            WriteLine("User hand = " + PrintCards(userCards));
            WriteLine("Opponent hand = " + PrintCards(oppCards));
            WriteLine(p);

            Server:
            Server server = new Server(int.Parse(args[0]));


//            int[] results = new int[3];
//            foreach (Card userCard in userCards)
//            {
//                deck.Remove(userCard);
//            }
//            foreach (Card card in desk)
//            {
//                deck.Remove(card);
//            }
//            List<Card> uHand = Probability.MergeCard(desk, userCards);
//            List<Card> oHand = new List<Card>(desk);
//            for (int i = 0; i < 100000; i++)
//            {
//                results[GenerateGame(deck, uHand, oHand) + 1]++;
//            }
//            for (int index = 0; index < pp.Length; index++)
//            {
//                pp[index] = pp[index] / 10000;
//            }
            Read();
        }

        private static int GenerateGame(List<Card> deck, List<Card> uHand, List<Card> oHand)
        {
            List<Card> curDeck = new List<Card>(deck);
            uHand = new List<Card>(uHand);
            oHand = new List<Card>(oHand);
            for (int i = uHand.Count; i < 7; i++)
            {
                int index = R.Next(0, curDeck.Count);
                Card c = curDeck[index];
                uHand.Add(c);
                oHand.Add(c);
                curDeck.RemoveAt(index);
            }
            for (int i = oHand.Count; i < 7; i++)
            {
                int index = R.Next(0, curDeck.Count);
                Card c = curDeck[index];
                oHand.Add(c);
                curDeck.RemoveAt(index);
            }
            Hand userHand = new Hand(uHand);
            Hand oppHand = new Hand(oHand);
            _pp[userHand.Weight[0]]++;
            return Hand.Compare(userHand, oppHand);
        }

        private static double[] _pp = new double[15];

        private static string PrintCards(List<Card> cards)
        {
            return cards.Aggregate("", (current, card) => current + (card + " "));
        }

        /// <summary>
        ///     Set default Deck with 52 cards
        /// </summary>
        public static void SetDeck()
        {
            for (byte i = 2; i <= 14; i++)
            {
                for (byte j = 1; j <= 4; j++)
                {
                    Deck.Add(new Card(i, j));
                }
            }

        }

        /// <summary>
        /// </summary>
        /// <returns>Random hand with HandSize cards</returns>
        public static Hand GetRandomHand(List<Card> deck, int size = HandSize, List<Card> h = null)
        {
            List<Card> curDeck = new List<Card>(Deck);
            Hand hand = h == null ? new Hand() : new Hand(h);
            for (int i = hand.Cards.Count; i < HandSize; i++)
            {
                int index = R.Next(0, curDeck.Count);
                Card c = curDeck[index];
                hand.AddCard(c);
                curDeck.RemoveAt(index);
            }
            return hand;
        }
    }
}