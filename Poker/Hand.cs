﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Poker
{
    [SuppressMessage("ReSharper", "InvertIf")]
    public class Hand : IEquatable<Hand>
    {
        public List<Card> Cards { get; }
        public int[] Weight { get; private set; }

        #region Constructors

        public Hand(List<Card> hand)
        {
            hand.Sort(Card.Comparer);
            Cards = new List<Card>(hand);
            Weight = new int[6];
            if (Cards.Count == 7)
                SetWeight();
        }

        public Hand(Hand hand)
        {
            Cards = new List<Card>(hand.Cards);
            Weight = hand.Weight.ToArray();
        }

        public Hand()
        {
            Weight = new int[6];
            Cards = new List<Card>();
        }

        #endregion Constructors

        public void AddCard(Card card)
        {
            Cards.Add(card);
            if (Cards.Count >= 7)
            {
                Cards.Sort(Card.Comparer);
                SetWeight();
            }
        }

        public void SetWeight()
        {
            if (IsStraightFlush())
            {
                return;
            }
            if (IsKare())
            {
                Weight[0] = 7;
                return;
            }
            if (IsFullHouse())
            {
                Weight[0] = 6;
                return;
            }
            if (Weight[0] != 4 && Weight[0] != 5)
                SetRemains();
        }

        public static int Compare(Hand x, Hand y)
        {
            int i = 0;
            while (x.Weight[i] == y.Weight[i])
            {
                i++;
                if (x.Weight.Length == i)
                    return 0;
            }

            if (x.Weight[i] > y.Weight[i])
                return 1;
            if (x.Weight[i] < y.Weight[i])
                return -1;
            return 0;
        }

        public static string GetCombination(int num)
        {
            switch (num)
            {
                case 9:
                    return "Flush Royal";
                case 8:
                    return "Straight Flush";
                case 7:
                    return "Kare";
                case 6:
                    return "Fullhouse";
                case 5:
                    return "Flush";
                case 4:
                    return "Straight";
                case 3:
                    return "Set";
                case 2:
                    return "Two Pairs";
                case 1:
                    return "Pair";
                default:
                    return "Kicker";
            }
        }

        #region Overrides

        public static bool operator ==(Hand h1, Hand h2)
        {
            return Compare(h1, h2) == 0;
        }

        public static bool operator !=(Hand h1, Hand h2)
        {
            return !(h1 == h2);
        }

        public static bool operator >(Hand h1, Hand h2)
        {
            return Compare(h1, h2) == 1;
        }

        public static bool operator <(Hand h1, Hand h2)
        {
            return Compare(h1, h2) == -1;
        }

        public override string ToString()
        {
            string s = GetCombination(Weight[0]) + " ";
            return Cards.Aggregate(s, (current, card) => current + (card + " "));
        }

        public bool Equals(Hand other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Cards, other.Cards) && Equals(Weight, other.Weight);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Hand) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Cards?.GetHashCode() ?? 0) * 397) ^ (Weight?.GetHashCode() ?? 0);
            }
        }

        #endregion Overrides

        #region Logic

        private bool IsStraightFlush()
        {
            int[] suit = new int[5];
            int[] faceCount = new int[15];
            bool hasStraight = false, hasFlush = false;
            foreach (Card card in Cards)
            {
                suit[card.Suit]++;
                faceCount[card.Face]++;
            }
            int maxIndex = Cards[0].Face;
            int maxSeq = 1;
            int seq = 1;
            int index = 0;

            for (int i = 2; i < maxIndex; i++)
            {
                if (faceCount[i] != 0 && faceCount[i + 1] != 0)
                    seq++;
                else
                    seq = 1;
                if (seq <= maxSeq) continue;
                index = i + 1;
                maxSeq = seq;
            }
            if (maxSeq >= Program.HandSize)
            {
                hasStraight = true;
                Weight[1] = index;
            }
            if (maxSeq == 4 && index == 5 && Cards[0].Face == 14)
            {
                hasStraight = true;
                Weight[1] = index;
            }

            int flushSuit = 0;
            int pos = 1;
            for (int i = suit.Length - 1; i >= 0; i--)
            {
                if (suit[i] < Program.HandSize) continue;
                flushSuit = i;
                break;
            }
            if (flushSuit != 0)
            {
                hasFlush = true;
                for (int i = 0; i < Cards.Count && pos < 6; i++)
                {
                    if (flushSuit == Cards[i].Suit)
                    {
                        Weight[pos] = Cards[i].Face;
                        pos++;
                    }
                }
            }
            int[] flushWeights = Weight.ToArray();
            if (hasFlush && hasStraight && CheckStraightFlush(flushSuit))
            {
                if (Weight[1] == 14)
                    Weight[0] = 9;
                else
                    Weight[0] = 8;
                return true;
            }
            if (hasFlush)
            {
                Weight = flushWeights;
                Weight[0] = 5;
            }
            else if (hasStraight)
                Weight[0] = 4;
            return false;
        }

        private bool CheckStraightFlush(int flushSuit)
        {
            int maxSeq = 1;
            int seq = 1;
            int top = 0;
            int maxIndex = Cards.Count;
            for (int i = maxIndex - 1; i > 0; i--)
            {
                if (Cards[i].Suit == flushSuit)
                {
                    int offset = 1;
                    while ((i - offset > 0) && Cards[i - offset].Suit != flushSuit)
                        offset++;
                    if (Cards[i].Face + 1 == Cards[i - offset].Face && Cards[i - offset].Suit == flushSuit)
                        seq++;
                    else if (Cards[i].Face == 5 && maxSeq == 4 && (
                                 (Cards[0].Face == 14 && Cards[0].Suit == flushSuit) ||
                                 (Cards[1].Face == 14 && Cards[1].Suit == flushSuit)
                             ))
                        seq++;
                    else
                        seq = 1;
                    if (seq > maxSeq)
                    {
                        top = Cards[i].Face;
                        maxSeq = seq;
                    }
                    i -= offset - 1;
                }
            }

            if (maxSeq < Program.HandSize)
                return false;
            Weight[1] = top;
            for (int i = 2; i < 6; i++)
            {
                Weight[i] = --top;
            }
            return true;
        }

        private bool IsKare()
        {
            return HasFhOrKare(4);
        }

        private bool IsFullHouse()
        {
            return HasFhOrKare(3);
        }

        private bool HasFhOrKare(int num)
        {
            int[] count = new int[15];
            foreach (Card card in Cards)
            {
                count[card.Face]++;
            }
            int firstFace = 0;
            int secondFace = 0;
            int maxIndex = Cards[0].Face;
            for (int i = maxIndex; i >= 2; i--)
            {
                if (count[i] == num)
                {
                    firstFace = i;
                    count[i] = 0;
                    break;
                }
            }
            for (int i = maxIndex; i >= 2; i--)
            {
                if (count[i] <= num && count[i] >= 5 - num)
                {
                    secondFace = i;
                    break;
                }
            }

            if (firstFace == 0 || secondFace == 0)
                return false;
            Weight[1] = firstFace;
            Weight[2] = secondFace;
            return true;
        }

        private void SetRemains()
        {
            int[] faceCount = new int[15];
            foreach (Card card in Cards)
            {
                faceCount[card.Face]++;
            }
            bool hasTwoPairs = false;
            int num = 1;
            int pos = 1;
            for (int i = faceCount.Length - 1; i >= 2; i--)
            {
                if (num == 2 && faceCount[i] == 2 && !hasTwoPairs)
                {
                    hasTwoPairs = true;
                    Weight[0] = 2;
                    Weight[2] = i;
                    pos++;
                } else if (num == 2 && faceCount[i] == 2 && hasTwoPairs)
                {
                    faceCount[i]--;
                }
                else if (faceCount[i] > num)
                {
                    num = faceCount[i];
                    if (num == 3)
                    {
                        Weight[0] = 3;
                        Weight[1] = i;
                        pos++;
                    }
                    else if (num == 2 && !hasTwoPairs)
                    {
                        Weight[0] = 1;
                        Weight[1] = i;
                        pos++;
                    }
                }
            }
            int weightEnd = 6 - num - (hasTwoPairs ? 1 : 0);
            for (int i = faceCount.Length - 1; i >= 0 && pos <= weightEnd; i--)
            {
                if (faceCount[i] == 1)
                {
                    Weight[pos] = i;
                    ++pos;
                }
            }
        }

        #endregion Logic
    }
}