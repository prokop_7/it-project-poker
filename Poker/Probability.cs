using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Poker
{
    public class Probability
    {
        private List<Card> User1Cards { get; }
        private List<Card> User2Cards { get; }
        private List<Card> Desk { get; }
        private List<Card> User1Deck { get; }
        private List<Card> User2Deck { get; }
        private List<Card> ServerDeck { get; }

        public double[] ServerProbabilityUser1 { get; set; } = new double[10];
        public double[] ServerProbabilityUser2 { get; set; } = new double[10];
        public int[] ServerCountUser1 { get; } = new int[10];
        public int[] ServerCountUser2 { get; } = new int[10];

        public int[] User1CountOpp { get; } = new int[10];
        public int[] User1CountUser { get; } = new int[10];
        public double[] User1ProbabilityUser { get; } = new double[10];
        public double[] User1ProbabilityOpp { get; } = new double[10];

        public int[] User2CountOpp { get; } = new int[10];
        public int[] User2CountUser { get; } = new int[10];
        public double[] User2ProbabilityUser { get; } = new double[10];
        public double[] User2ProbabilityOpp { get; } = new double[10];

        public double[] WinServer { get; } = new double[3];
        public double[] WinUser1 { get; } = new double[3];
        public double[] WinUser2 { get; } = new double[3];

        public int[] WinUser1Count { get; set; } = new int[3];
        public int[] WinUser2Count { get; set; } = new int[3];
        public int[] WinServerCount { get; set; } = new int[3];

        private int _counterUser1;
        private int _counterUser2;
        private int _counterServer;


        public Probability(List<Card> uCards, List<Card> desk, List<Card> deck, List<Card> oCards)
        {
            User1Cards = new List<Card>(uCards);
            User2Cards = new List<Card>(oCards);
//            User1Deck = new List<Card>(deck);
//            User2Deck = new List<Card>(deck);
            Desk = new List<Card>(desk);
            User1Deck = MergeCard(deck, oCards);
            User2Deck = MergeCard(deck, uCards);
            foreach (Card card in desk)
            {
                User1Deck.Remove(card);
                User2Deck.Remove(card);
            }
            foreach (Card user1Card in User1Cards)
            {
                User1Deck.Remove(user1Card);
            }

            ServerDeck = new List<Card>(User1Deck);
            foreach (Card user2Card in User2Cards)
            {
                ServerDeck.Remove(user2Card);
                User2Deck.Remove(user2Card);
            }
            Thread t1 = new Thread(() => CalculateAsyncUser(true));
            Thread t2 = new Thread(() => CalculateAsyncUser(false));
            t1.Start();
            t2.Start();
            CountProbabilitiesServer(ServerDeck, 7, new Hand(MergeCard(User1Cards, Desk)), null,
                MergeCard(User2Cards, Desk));
            CalculateProbabilities(false);
            CountProbabilities(MergeCard(User1Deck, User2Cards), 7, new Hand(MergeCard(User1Cards, Desk)),
                new List<Card>(Desk));
            CountProbabilities(MergeCard(User2Deck, User1Cards), 7, new Hand(MergeCard(User2Cards, Desk)),
                new List<Card>(Desk), false);
            t2.Join();
            t1.Join();
            CalculateProbabilities();
        }

        private void CalculateProbabilities(bool isUser = true)
        {
            if (isUser)
            {
                CalculateProbabilitiesFor(User1ProbabilityOpp, User1ProbabilityUser, User1CountOpp, User1CountUser,
                    WinUser1, WinUser1Count, out _counterUser1);
                CalculateProbabilitiesFor(User2ProbabilityOpp, User2ProbabilityUser, User2CountOpp, User2CountUser,
                    WinUser2, WinUser2Count, out _counterUser2);
            }
            else
                CalculateProbabilitiesFor(ServerProbabilityUser2, ServerProbabilityUser1, ServerCountUser2,
                    ServerCountUser1,
                    WinServer, WinServerCount, out _counterServer);
        }


        public void CalculateProbabilitiesFor(double[] probabilityOpp, double[] probability, int[] countOpp,
            int[] countUser, double[] win, int[] winCount, out int counter)
        {
            double count = countOpp.Sum();
            for (int i = 0; i < 10; i++)
            {
                probabilityOpp[i] = countOpp[i] / count;
            }
            count = countUser.Sum();
            if (Math.Abs(count) < 0.5)
            {
                probability[new Hand(MergeCard(win == WinUser1 ? User1Cards : User2Cards, Desk)).Weight[0]] = 1;
                countUser[new Hand(MergeCard(win == WinUser1 ? User1Cards : User2Cards, Desk)).Weight[0]] = 1;
            }
            else
                for (int i = 0; i < 10; i++)
                {
                    probability[i] = countUser[i] / count;
                }
            counter = winCount.Sum();
            win[0] = winCount[0] / (double) counter;
            win[1] = winCount[1] / (double) counter;
            win[2] = winCount[2] / (double) counter;
        }

        public List<List<Card>> CountProbabilitiesServer(
            List<Card> deck, // DECK
            int size, // SIZE OF HAND = 7
            Hand userHand, // Handuser for compare
            List<List<Card>> cards = null, //
            List<Card> oppCards = null) // Hand which will pumped
        {
            if (oppCards == null)
                oppCards = new List<Card>();
            if (cards == null)
                cards = new List<List<Card>>();
            List<Card> curDeck = new List<Card>(deck);

            if (oppCards.Count >= size)
            {
                if (userHand.Cards.Count != size) return cards;
                Hand oppHand = new Hand(oppCards);
                ServerCountUser2[oppHand.Weight[0]]++;
                cards.Add(oppCards);
                switch (Hand.Compare(userHand, oppHand))
                {
                    case -1:
                        WinServerCount[2]++;
                        break;
                    case 1:
                        WinServerCount[0]++;
                        break;
                    case 0:
                        WinServerCount[1]++;
                        break;
                }
                return cards;
            }
            int lenght = curDeck.Count;
            if (lenght == 0)
                return cards;
            for (int i = 0; i < lenght; i++)
            {
                Card item = curDeck[0];
                List<Card> set = new List<Card>(oppCards) {item};
                curDeck.RemoveAt(0);
                if (userHand.Cards.Count < size)
                {
                    Hand curUserHand = new Hand(userHand.Cards);
                    curUserHand.AddCard(item);
                    if (curUserHand.Cards.Count == size)
                        ServerCountUser1[curUserHand.Weight[0]]++;
                    cards = CountProbabilitiesServer(curDeck, size, curUserHand, cards, set);
                }
                else
                    cards = CountProbabilitiesServer(curDeck, size, userHand, cards, set);
            }
            return cards;
        }


        [MTAThread]
        public void CountProbabilities(
            List<Card> deck, // DECK
            int size, // SIZE OF HAND = 7
            Hand userHand, // Handuser for compare
            List<Card> oppCards = null,
            bool isFirstUser = true) // Hand which will pumped
        {
            if (oppCards == null)
                oppCards = new List<Card>();
            List<Card> curDeck = new List<Card>(deck);

            if (oppCards.Count >= size)
            {
                if (userHand.Cards.Count != size) return;
                Hand oppHand = new Hand(oppCards);
                if (isFirstUser)
                    User1CountOpp[oppHand.Weight[0]]++;
                else
                    User2CountOpp[oppHand.Weight[0]]++;
                return;
            }
            int lenght = curDeck.Count;
            if (lenght == 0)
                return;
            for (int i = 0; i < lenght; i++)
            {
                Card item = curDeck[0];
                List<Card> set = new List<Card>(oppCards) {item};
                curDeck.RemoveAt(0);
                if (userHand.Cards.Count < size)
                {
                    Hand curUserHand = new Hand(userHand.Cards);
                    curUserHand.AddCard(item);
                    if (curUserHand.Cards.Count == size)
                    {
                        if (isFirstUser)
                            User1CountUser[curUserHand.Weight[0]]++;
                        else
                            User2CountUser[curUserHand.Weight[0]]++;
                    }
                    CountProbabilities(curDeck, size, curUserHand, set, isFirstUser);
                }
                else
                    CountProbabilities(curDeck, size, userHand, set, isFirstUser);
            }
        }

        public List<List<Card>> GetAllCombinations(
            List<Card> deck,
            int size,
            List<List<Card>> cards = null,
            List<Card> curSet = null)
        {
            if (curSet == null)
                curSet = new List<Card>();
            if (cards == null)
                cards = new List<List<Card>>();
            List<Card> curDeck = new List<Card>(deck);

            if (curSet.Count >= size)
            {
                cards.Add(curSet);
                return cards;
            }
            int lenght = curDeck.Count;
            if (lenght == 0)
                return cards;
            for (int i = 0; i < lenght; i++)
            {
                Card item = curDeck[0];
                List<Card> set = new List<Card>(curSet) {item};
                curDeck.RemoveAt(0);
                cards = GetAllCombinations(curDeck, size, cards, set);
            }
            return cards;
        }


        [MTAThread]
        private void PartialCounter(
            List<Card> add,
            List<Card> deckUser, // DECK user
            List<Card> desk, bool isFirstUser)
        {
            int lenght = add.Count;
            List<Card> curDeck = new List<Card>(deckUser);
            if (desk.Count >= 5) return;
            for (int i = 0; i < lenght; i++)
            {
                List<Card> curDesk = new List<Card>(desk);
                Card item = add[i];
                curDeck.Remove(item);
                curDesk.Add(item);
                var userCards = isFirstUser ? User1Cards : User2Cards;
                var uCards = MergeCard(userCards, curDesk);
                CountProbabilitiesUser(curDeck, uCards, curDesk, isFirstUser);
            }
        }

        private void CalculateAsyncUser(bool isFirstUser)
        {
            if (Desk.Count == 5)
            {
                if (isFirstUser)
                    CountProbabilitiesUser(MergeCard(User1Deck, User2Cards), MergeCard(User1Cards, Desk), Desk, true);
                else
                    CountProbabilitiesUser(MergeCard(User2Deck, User1Cards), MergeCard(User2Cards, Desk), Desk, false);
                return;
            }
            int procNums = Environment.ProcessorCount;
            List<Card>[] lists = new List<Card>[procNums];
            List<Card>[] decks = new List<Card>[procNums];
            Thread[] threads = new Thread[procNums];
            List<Card> deck = isFirstUser ? User1Deck : User2Deck;
            int k = deck.Count / procNums;
            for (int i = 0; i < procNums; i++)
            {
                lists[i] = new List<Card>(deck.GetRange(i * k, (i + 1) == procNums ? deck.Count - i * k : k));
                decks[i] = new List<Card>(deck.GetRange(i * k, deck.Count - i * k));
                int i1 = i;
                threads[i] = new Thread(() => PartialCounter(lists[i1], decks[i1], Desk, isFirstUser));
                threads[i].Start();
            }

            foreach (Thread thread in threads)
                thread.Join();
        }

        public void CountProbabilitiesUser(
            List<Card> deckUser, // DECK user
            List<Card> userCards,
            List<Card> desk,
            bool isFirstUser) // Hand which will pumped
        {
            if (userCards.Count == 7)
            {
                List<Card> dDeck = isFirstUser ? new List<Card>(User1Deck) : new List<Card>(User2Deck);
                foreach (Card card in desk)
                {
                    dDeck.Remove(card);
                }
                Calc(dDeck, new Hand(userCards), desk, isFirstUser);
                return;
            }
            if (deckUser.Count == 0)
                return;
            int lenght = deckUser.Count;
            List<Card> curDeck = new List<Card>(deckUser);
            for (int i = 0; i < lenght; i++)
            {
                Card item = curDeck[0];
                List<Card> curUserCards = new List<Card>(userCards) {item};
                List<Card> curDesk = new List<Card>(desk) {item};
                curDeck.RemoveAt(0);
                CountProbabilitiesUser(curDeck, curUserCards, curDesk, isFirstUser);
            }
        }

        [MTAThread]
        private void Calc(List<Card> deck, Hand uHand, List<Card> oppCards, bool isFirstUser)
        {
            if (oppCards.Count == 7)
            {
                Hand oppHand = new Hand(oppCards);
                Monitor.Enter(WinUser1Count);
                Monitor.Enter(WinUser2Count);
                switch (Hand.Compare(uHand, oppHand))
                {
                    case -1:
                        if (isFirstUser)
                            WinUser1Count[2]++;
                        else
                            WinUser2Count[2]++;
                        break;
                    case 1:
                        if (isFirstUser)
                            WinUser1Count[0]++;
                        else
                            WinUser2Count[0]++;
                        break;
                    case 0:
                        if (isFirstUser)
                            WinUser1Count[1]++;
                        else
                            WinUser2Count[1]++;
                        break;
                }
                Monitor.Exit(WinUser1Count);
                Monitor.Exit(WinUser2Count);
                return;
            }
            int lenght = deck.Count;
            List<Card> curDeck = new List<Card>(deck);
            for (int i = 0; i < lenght; i++)
            {
                Card item = curDeck[0];
                List<Card> curOppCards = new List<Card>(oppCards) {item};
                curDeck.RemoveAt(0);
                Calc(curDeck, uHand, curOppCards, isFirstUser);
            }
        }

        /// <summary>
        ///  Merge two List of Cards
        /// </summary>
        /// <param name="first">First list</param>
        /// <param name="second">Second list</param>
        /// <returns>new list of cards</returns>
        public static List<Card> MergeCard(List<Card> first, List<Card> second)
        {
            List<Card> cards = new List<Card>(first);
            foreach (Card card in second)
            {
                cards.Remove(card);
            }
            cards.AddRange(second);
            return cards;
        }

        public override string ToString()
        {
            const int defLenght = 60;
            string s = AddLine(defLenght * 2);
            int longestLine = s.Length;
            s += AddSpace(" User probability:", defLenght / 2, false) +
                 AddSpace("#outcomes = " + _counterUser1, defLenght / 2) +
                 AddSpace(" Real probability:", defLenght / 2, false) +
                 AddSpace("#outcomes = " + _counterServer, defLenght / 2) + "\n";
            s += AddSpace(" Win  = " + Math.Round(WinUser1[0] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Win  = " + _counterUser1 * WinUser1[0], defLenght / 2);
            s += AddSpace(" Win  = " + Math.Round(WinServer[0] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Win  = " + _counterServer * WinServer[0], defLenght / 2) + "\n";
            s += AddSpace(" Draw = " + Math.Round(WinUser1[1] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Draw = " + _counterUser1 * WinUser1[1], defLenght / 2);
            s += AddSpace(" Draw = " + Math.Round(WinServer[1] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Draw = " + _counterServer * WinServer[1], defLenght / 2) + "\n";
            s += AddSpace(" Lose = " + Math.Round(WinUser1[2] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Lose = " + _counterUser1 * WinUser1[2], defLenght / 2);
            s += AddSpace(" Lose = " + Math.Round(WinServer[2] * 100, 4) + "%", defLenght / 2, false) +
                 AddSpace("#Lose = " + _counterServer * WinServer[2], defLenght / 2) + "\n";
            s += AddLine(defLenght * 2);
            s += AddSpace("Probability for certain combination", longestLine, false, true) + "\n";
            s += AddSpace("User probability:", defLenght, onCentre: true) +
                 AddSpace("Real probability:", defLenght, onCentre: true) +
                 "\n" + AddSpace("", defLenght) + AddSpace("", defLenght) + "\n";
            s += AddSpace(" User:", defLenght / 2) + AddSpace(" Opponent:", defLenght / 2) +
                 AddSpace(" User:", defLenght / 2) + AddSpace(" Opponent:", defLenght / 2) +
                 "\n";

            //Print probabilities
            for (int i = User1ProbabilityUser.Length - 1; i >= 0; i--)
            {
                string row = " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                             Math.Round(User1ProbabilityUser[i] * 100, 2) + "%";
                row = AddSpace(row, defLenght / 2);
                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       Math.Round(User1ProbabilityOpp[i] * 100, 2) + "%";
                row = AddSpace(row, defLenght);

                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       Math.Round(ServerProbabilityUser1[i] * 100, 2) + "%";
                row = AddSpace(row, defLenght * 3 / 2);
                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       Math.Round(ServerProbabilityUser2[i] * 100, 2) + "%";
                row = AddSpace(row, defLenght * 2) + "\n";
                s += row;
            }
            s += AddLine(defLenght * 2);
            s += "\n";
            //Print count
            for (int i = User1ProbabilityUser.Length - 1; i >= 0; i--)
            {
                string row = " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                             User1CountUser[i];
                row = AddSpace(row, defLenght / 2);
                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       User1CountOpp[i];
                row = AddSpace(row, defLenght);

                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       ServerCountUser1[i];
                row = AddSpace(row, defLenght * 3 / 2);
                row += " " + AddSpace(Hand.GetCombination(i), 15, false) + "= " +
                       ServerCountUser2[i];
                row = AddSpace(row, defLenght * 2) + "\n";
                s += row;
            }
            {
                string row = AddSpace("", defLenght / 2);
                row = AddSpace(row, defLenght);
                row = AddSpace(row, defLenght * 3 / 2);
                row = AddSpace(row, defLenght * 2) + "\n";
                s += row;
            }
            {
                string row = " " + AddSpace("#Outcomes", 15, false) + "= " +
                             User1CountUser.Sum();
                row = AddSpace(row, defLenght / 2);
                row += " " + AddSpace("#Outcomes", 15, false) + "= " +
                       User1CountOpp.Sum();
                row = AddSpace(row, defLenght);

                row += " " + AddSpace("#Outcomes", 15, false) + "= " +
                       ServerCountUser1.Sum();
                row = AddSpace(row, defLenght * 3 / 2);
                row += " " + AddSpace("#Outcomes", 15, false) + "= " +
                       ServerCountUser2.Sum();
                row = AddSpace(row, defLenght * 2) + "\n";
                s += row;
            }
            s += AddLine(defLenght * 2);


            return s;
        }

        private static string AddSpace(string s, int length, bool addBorder = true, bool onCentre = false)
        {
            int initialLenght = s.Length;
            string spaces = "";
            if (onCentre)
                for (int i = 0; i < (length - initialLenght) / 2; i++)
                    spaces += " ";
            s = spaces + s;
            while (s.Length < length - 1)
                s += " ";
            if (addBorder)
                s += "|";
            else s += " ";
            return s;
        }

        private static string AddLine(int length)
        {
            string s = "";
            for (int i = 0; i < length; i++)
            {
                s += "_";
            }
            s += "\n";
            return s;
        }
    }
}