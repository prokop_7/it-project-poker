﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Mime;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace Poker
{
    [SuppressMessage("ReSharper", "StringIndexOfIsCultureSpecific.1")]
    class Server
    {
        public static readonly Random R = new Random();
        public static int Turn { get; set; } = 0;
        public static List<Card> Hand1 { get; set; } = new List<Card>();
        public static List<Card> Hand2 { get; set; } = new List<Card>();
        public static List<Card> Desk { get; set; } = new List<Card>();
        public static List<Card> Deck { get; set; } = new List<Card>(Program.Deck);
        private static Probability[] CurrentProbArr { get; set; } = new Probability[3];
        private static Probability CurrentProb { get; set; }

        public static List<Card> NextDeck { get; set; } = new List<Card>(Program.Deck);
        public static List<Card> NextHand1 { get; } = new List<Card>();
        public static List<Card> NextHand2 { get; } = new List<Card>();
        public static List<Card> NextDesk { get; } = new List<Card>();
        private static Probability[] NextProbArr { get; set; } = new Probability[3];

        private static Thread _thread;

        private static Socket Socket { get; set; }


        public Server(int port)
        {
            StartGame();
            StartListening(port);
        }

        ~Server()
        {
            if (Socket == null || Socket.Blocking) return;
            byte[] msg = Encoding.ASCII.GetBytes("Server was stopped");
            Socket.Send(msg);
            Socket.Shutdown(SocketShutdown.Both);
            Socket.Close();
        }

        private static void Handler(string requestUri)
        {
            switch (requestUri)
            {
                case "test":
                    SendResponse(new {Result = 1, Data = new {test = "TSS"}, ErrorDescription = ""});
                    break;

                case "getCardsOnTable":
                    int range = Turn > 0 ? Turn + 2 : 0;
                    SendResponse(new {Result = 1, Data = Desk.GetRange(0, range), ErrorDescription = ""});
                    break;

                case "getCardsOnHand1":
                    GetHand(Hand1);
                    break;

                case "getCardsOnHand2":
                    GetHand(Hand2);
                    break;

                case "addCards":
                    if (Turn == 3)
                    {
                        BadResponse("Desk is full");
                        break;
                    }
                    CurrentProb = CurrentProbArr[Turn];
                    Turn++;
                    OkResponse();
                    break;

                case "getProbability":
                    SendResponse(new {Result = 1, Data = CurrentProb, ErrorDescription = ""});
                    break;

                case "getProbUser1":
                    if (Turn == 0)
                    {
                        BadResponse("Add cards to desk");
                        break;
                    }
                    SendProbUser(true);
                    break;

                case "getProbUser2":
                    if (Turn == 0)
                    {
                        BadResponse("Add cards to desk");
                        break;
                    }
                    SendProbUser(false);
                    break;

                case "getWinUser1":
                    SendProbWin(true);
                    break;

                case "getWinUser2":
                    SendProbWin(false);
                    break;

                case "":
                    BadResponse("Empty request");
                    break;

                case "newGame":
                    _thread?.Join();
                    Turn = 0;

                    Hand1 = new List<Card>(NextHand1);
                    Hand2 = new List<Card>(NextHand2);
                    Desk = new List<Card>(NextDesk);
                    Deck = new List<Card>(NextDeck);

                    NextHand1.Clear();
                    NextHand2.Clear();
                    NextDesk.Clear();
                    NextDeck = new List<Card>(Program.Deck);
                    GenerateHands();
                    CurrentProbArr = NextProbArr;
                    NextProbArr = new Probability[3];
                    OkResponse();
                    _thread = new Thread(() => GenerateProb(NextProbArr));
                    _thread.Start();
                    break;

                case "stop":
                    SendResponse(new
                    {
                        Result = 1,
                        Data = "Initialising server termination...\nAborting connection...\nExiting environment...",
                        ErrorDescription = ""
                    });
                    Environment.Exit(0);
                    break;

                default:
                    BadResponse("Unknown request");
                    break;
            }
        }

        private static void StartGame()
        {
            GenerateHands();
            GenerateProb(NextProbArr);
            Hand1 = new List<Card>(NextHand1);
            Hand2 = new List<Card>(NextHand2);
            Desk = new List<Card>(NextDesk);
            NextHand1.Clear();
            NextHand2.Clear();
            NextDesk.Clear();
            NextDeck = new List<Card>(Program.Deck);
            CurrentProb = null;
            CurrentProbArr = NextProbArr;
            NextProbArr = new Probability[3];
            GenerateHands();
            GenerateProb(NextProbArr);
        }

        private static void GenerateProb(Probability[] nextProbArr)
        {
            Monitor.Enter(nextProbArr);
            AddCard(NextDesk);
            AddCard(NextDesk);
            AddCard(NextDesk);
            nextProbArr[0] = new Probability(NextHand1, NextDesk, NextDeck, NextHand2);
            AddCard(NextDesk);
            nextProbArr[1] = new Probability(NextHand1, NextDesk, NextDeck, NextHand2);
            AddCard(NextDesk);
            nextProbArr[2] = new Probability(NextHand1, NextDesk, NextDeck, NextHand2);
            Monitor.Exit(nextProbArr);
        }

        private static void SendProbUser(bool isFirstUser)
        {
            object prob = isFirstUser
                ? new
                {
                    CombinationUser1 = CurrentProb.User1ProbabilityUser,
                    CombinationUser2 = CurrentProb.User1ProbabilityOpp
                }
                : new
                {
                    CombinationUser1 = CurrentProb.User2ProbabilityOpp,
                    CombinationUser2 = CurrentProb.User2ProbabilityUser
                };
            SendResponse(new {Result = 1, Data = prob, ErrorDescription = ""});
        }

        private static void SendProbWin(bool isFirstUser)
        {
            object prob = isFirstUser ? new {WinProb = CurrentProb.WinUser1} : new {WinProb = CurrentProb.WinUser2};
            SendResponse(new {Result = 1, Data = prob, ErrorDescription = ""});
        }

        private static void OkResponse()
        {
            SendResponse(new {Result = 1, Data = "", ErrorDescription = ""});
        }

        private static void BadResponse(string description)
        {
            SendResponse(new {Result = 0, Data = "", ErrorDescription = description});
        }

        private static void GetHand(List<Card> hand)
        {
            SendResponse(new {Result = 1, Data = hand, ErrorDescription = ""});
        }

        public static void StartListening(int port)
        {
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            while (true)
                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(10);
                    Console.WriteLine(localEndPoint);
                    while (true)
                    {
                        Socket = listener.Accept();
                        string data = null;

                        while (true)
                        {
                            byte[] bytes = new byte[1024];
                            int bytesRec = Socket.Receive(bytes);
                            data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                            if (data.IndexOf("<EOF>") > -1)
                                break;
                        }
                        data = data.Replace("<EOF>", string.Empty);
                        Console.WriteLine("{1:HH:mm:ss.fff}  -  Text received : {0}", data, DateTime.Now);
                        Handler(data);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    Socket.Shutdown(SocketShutdown.Both);
                    Socket.Close();
                }
        }

        private static void SendResponse(object response)
        {
            SendResponse(JsonConvert.SerializeObject(response));
        }

        private static void SendResponse(string response)
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + $"  -       Response : {response}\n");
            byte[] buffer = Encoding.ASCII.GetBytes(response);
            Socket.Send(buffer);
        }

        private static void GenerateHands()
        {
            for (int i = 0; i < 2; i++)
            {
                AddCard(NextHand1);
                AddCard(NextHand2);
            }
        }

        private static void AddCard(List<Card> hand)
        {
            int index = R.Next(0, NextDeck.Count);
            hand.Add(NextDeck[index]);
            NextDeck.RemoveAt(index);
        }
    }
}