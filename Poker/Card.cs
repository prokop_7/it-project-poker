﻿using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public struct Card
    {
        public static readonly Dictionary<int, string> CardFace = new Dictionary<int, string>()
        {
            {2, "2"},
            {3, "3"},
            {4, "4"},
            {5, "5"},
            {6, "6"},
            {7, "7"},
            {8, "8"},
            {9, "9"},
            {10, "10"},
            {11, "J"},
            {12, "Q"},
            {13, "K"},
            {14, "A"},
        };

        private static byte StringToFace(string s)
        {
            return
                (byte)
                (from keyValuePair in CardFace where keyValuePair.Value == s select keyValuePair.Key).FirstOrDefault();
        }

        private static byte StringToSuit(string s)
        {
            return
                (byte)
                (from keyValuePair in CardSuit where keyValuePair.Value == s select keyValuePair.Key).FirstOrDefault();
        }

        public static readonly Dictionary<int, string> CardSuit = new Dictionary<int, string>()
        {
            {1, "♠"},
            {2, "♥"},
            {3, "♣"},
            {4, "♦"}
        };

        public Card(byte face, byte suit)
        {
            Face = face;
            Suit = suit;
        }

        public Card(string s)
        {
            Face = StringToFace(s.Substring(0, s.Length - 1));
            Suit = StringToSuit(s[s.Length - 1].ToString());
        }

        public byte Face { get; }
        public byte Suit { get; }

        public override string ToString()
        {
            return CardFace[Face] + CardSuit[Suit];
        }

        public static int Comparer(Card a, Card b)
        {
            if (a.Face > b.Face)
                return -1;
            return a.Face < b.Face ? 1 : 0;
        }
    }
}